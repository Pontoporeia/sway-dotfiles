# sway-dotfiles

My sway configuration, dotfiles and scripts

## Packages
Install `yay` with the script in my sway [https://codeberg.org/Pontoporeia/dotfiles](https://codeberg.org/Pontoporeia/dotfiles).

Install `chezmoi`

```
sudo pacman -S chezmoi firefox foot nemo bitwarden evince \
nemo-fileroller nemo-preview nemo-share \
ark micro thunderbird signal-desktop element-desktop \
tldr python-pipx smartmontools qbittorrent nextcloud-client \
flatpak detox gnome-disk-utility wev cargo croc bluez bluez-utils btop yt-dlp \
mpv pavucontrol imv grim brightnessctl git wl-clipboard \
notepadqq kakune neovim 
ttf-arimo-nerd ttf-bitstream-vera ttf-dejavu \
ttf-font-awesome ttf-iosevka-nerd ttf-jetbrains-mono-nerd \
ttf-liberation ttf-nerd-fonts-symbols ttf-opensans \
ttf-overpass ttf-roboto-mono-nerd ttf-ubuntu-nerd \
ttf-ubuntu-mono-nerd \
kvantum qt5ct qt6ct otf-font-awesome papirus-icon-theme \
materia-kde materia-gtk-theme kvantum-theme-materia \
&& yay -S nwg-look libre-menu-editor appimagelauncher webapp-manager \
bibata-cursor-theme wallust cava tofi
```

- [ ] finish installing kwallet or gnome-keyring or another
- [ ] setup sddm with minimalist theme or greetd or just tty

